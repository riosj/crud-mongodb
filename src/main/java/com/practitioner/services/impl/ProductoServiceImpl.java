package com.practitioner.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practitioner.model.ProductoModel;
import com.practitioner.repository.ProductoRepository;
import com.practitioner.services.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService {

	@Autowired
	private ProductoRepository productoRepository;

	public List<ProductoModel> list() {
		return productoRepository.findAll();
	}

	public ProductoModel get(String id) {
		final Optional<ProductoModel> optional = productoRepository.findById(id);
		return optional.isPresent() ? optional.get() : null;
	}

	public ProductoModel save(ProductoModel u) {
		return productoRepository.save(u);
	}

	public void delete(String id) {
		productoRepository.deleteById(id);
	}
}
