package com.practitioner.services;

import java.util.List;

import com.practitioner.model.ProductoModel;

public interface ProductoService {

	List<ProductoModel> list();

	ProductoModel get(String id);

	ProductoModel save(ProductoModel u);

	void delete(String id);

}