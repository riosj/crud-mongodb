package com.practitioner.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practitioner.model.ProductoModel;
import com.practitioner.services.ProductoService;

@RestController
@RequestMapping("productos")
public class ProductoController {

	@Autowired
	private ProductoService productoService;

	@GetMapping
	public ResponseEntity<List<ProductoModel>> list() {
		return ResponseEntity.ok(productoService.list());
	}

	@GetMapping("/{id}")
	public ResponseEntity<ProductoModel> get(@PathVariable String id) {
		ProductoModel usuario = productoService.get(id);
		if (usuario == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.ok(usuario);
	}

	@PostMapping
	public ResponseEntity<String> add(@RequestBody ProductoModel u) {
		productoService.save(u);
		return ResponseEntity.status(HttpStatus.CREATED).body("Producto creado");
	}

	@PutMapping
	public ResponseEntity update(@RequestBody ProductoModel usuario) {
		ProductoModel u = productoService.get(usuario.getId());
		if (u == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		productoService.save(usuario);
		return ResponseEntity.ok("Producto actualizado");
	}

	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable String id) {
		ProductoModel u = productoService.get(id);
		if (u == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		productoService.delete(id);
		return ResponseEntity.noContent().build();
	}

}
