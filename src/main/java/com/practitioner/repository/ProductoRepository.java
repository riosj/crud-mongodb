package com.practitioner.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.practitioner.model.ProductoModel;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {
}
